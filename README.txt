Drupal watchdog cleaner module:
------------------------------
Requires - Drupal 6

Overview:
--------
This module provide a basic control tool for drupal logging system.
You can simply clear all logged data and reset log index id, or simply enable
periodic clean to clear old log entries.

Note: 
----
This module work with drupal cron, you should set up drupal cron.
see http://drupal.org/cron

Installation:
------------
1. Place this module directory in your modules folder (this will
   usually be "sites/all/modules/").
2. Go to "Administer" -> "Site building" -> "Modules" and enable the module.
3. Go to "Administer -> Site configuration -> Watchdog Cleaner" for
   configuration.
